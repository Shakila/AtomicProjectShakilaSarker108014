<?php
include_once("../../../"."vendor/autoload.php");

use \App\Bitm\SEIP108014\Terms_and_conditions;

$obj = new Terms_and_conditions();
$thetc = $obj->edit($_GET['id'],$_GET['status']);
?>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Levels</title>
        <link rel="stylesheet" type="text/css" href="../../../css/birthdayStyle.css">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="wrapper col-md-12 col-xs-4 col-sm-8">
                     <div class="home col-sm-12">
                        <a href="index.php"><img src="../../../images/icon-home.png"/></a>
                    </div>
                    
                    <div class ="title">
                        <h2>Levels</h2>
                    </div>
                        <form action="update.php" method="post" class="form-horizontal" role="form">
                            <input type="hidden" name="id" value="<?php echo $thetc->id;?>" /> 
                            <div class="form-group">
                                <div class="radio">
                            <label><input type="radio" name="status" value="Agree" <?php if($thetc->status=='Agree') echo "checked" ?>>Agree</label>
                            <label><input type="radio" name="status" value="Not Agree" <?php if($thetc->status=='Not Agree') echo "checked" ?>>Not Agree</label>
                        </div>
                            </div>
                           
                            <div class="form-group">        
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-sucsess">Submit</button>
                                </div>
                            </div>
                        </form>
                  
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>

