<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Profile_picture;

$obj = new Profile_picture();
$ppp = $obj->show($_GET['id']);
?>
<html>
    <head>
        <title>View Educational level</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <table>
            <tr>

                <td>ID: </td>
                <td><?php echo $ppp->id; ?> </td>

            </tr>
            <br>
            <tr>
                <td>
                    <?php
                    if ($ppp->picture != null) {
                        echo " <img src=\"uploaded/{$ppp->id}.{$ppp->picture}\" width = '200' height = '200' >";
                    } else {
                        echo "no image";
                    }
                    ?>
                </td>
            </tr>

        </table>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>
