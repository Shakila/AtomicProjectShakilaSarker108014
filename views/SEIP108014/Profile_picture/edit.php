<?php
session_start();
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Profile_picture;

$pic = new Profile_picture();
$thepic = $pic->edit($_GET['id']);
//var_dump($thepic);
//die();
?>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Edit Picture</title>

        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="wrapper col-md-12 col-xs-4 col-sm-8">
                    <div class="home col-sm-12">
                        <a href="index.php"><img src="../../../images/icon-home.png"/></a>
                    </div>

                    <div class ="title">
                        <h2>Profile Pictures</h2>
                    </div>
                    <form action="update.php" method="post" class="form-horizontal" enctype="multipart/form-data" role="form">
                        <input type="hidden" name="id" value="<?php echo $thepic->id; ?>" /> 
                        <div class="col-sm-6">
                            <input type="file" class="form-control" name="pic" id="pic"><?php  echo " <img src=\"uploaded/{$thepic->id}.{$thepic->picture}\" width = '50' height = '50' >"; ?>
                        </div>
                        <div class="form-group">        
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-sucsess">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>