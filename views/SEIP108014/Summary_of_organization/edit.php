<?php
include_once("../../../"."vendor/autoload.php");

use \App\Bitm\SEIP108014\Summary_of_organization;

$obj = new Summary_of_organization();
$thesum = $obj->edit($_GET['id']);
?>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Summary</title>
        <link rel="stylesheet" type="text/css" href="../../../css/birthdayStyle.css">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="wrapper col-md-12 col-xs-4 col-sm-8">
                     <div class="home col-sm-12">
                        <a href="index.php"><img src="../../../images/icon-home.png"/></a>
                    </div>
                    
                    <div class ="title">
                        <h2>Birthdays</h2>
                    </div>
                        <form action="update.php" method="post" class="form-horizontal" role="form">
                            <input type="hidden" name="id" value="<?php echo $thesum->id;?>" /> 
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">Name:</label>
                                <div class="col-sm-6">          
                                    <input type="text" class="form-control" name="name" value="<?php echo $thesum->name; ?>" id="name" placeholder="Enter your name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="summary">Summary:</label>
                                <div class="col-sm-6">
                                    <textarea class="form-control" name="summary" id="summary"><?php echo $thesum->summary; ?></textarea>
                                </div>
                            </div>
                           
                            <div class="form-group">        
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-sucsess">Submit</button>
                                </div>
                            </div>
                        </form>
                  
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>

