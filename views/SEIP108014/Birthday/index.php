<?php
session_start();
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Birthday;

$obj = new Birthday();
$var = $obj->index();
?>
<html>
       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">

              <title>Birthday List</title>

              <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
              <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

       </head>
       <body>
              <div class="container">
                     <div class="row">   
                            <div class="wrapper col-md-8 col-sm-4 col-xs-2">

                                   <div class ="title">
                                          <h2>Birthdays</h2>
                                   </div> 
                                   <div class="col-md-6 col-sm-2 col-xs-1">
                                          <a href="create.php">Create Birthdays</a>
                                   </div>
                                   <table class="table table-striped">
                                          <thead>
                                                 <tr>
                                                        <th>Serial no</th>
                                                        <th>Name</th>
                                                        <th>Birth Date</th>
                                                        <th>Action</th>
                                                 </tr>
                                          </thead>
                                          <tbody>
                                              <?php
                                              $sl = 0;
                                              foreach ($var as $birthdays):
                                                  $sl++;
                                                  ?>
                                                     <tr>

                                                            <td><?php echo $sl; ?></td>
                                                            <td><?php echo $birthdays['name']; ?></td>
                                                            <td><?php echo $birthdays['birth_date']; ?></td>
                                                            <td><a href="edit.php?id=<?php echo $birthdays['id']; ?>">Update </a><?php echo '| '; ?><a href="show.php?id=<?php echo $birthdays['id']; ?>">View </a><?php echo '| '; ?>
                                                                   <form action="delete.php" method="post">
                                                                          <input type="hidden" name="id" value="<?php echo $birthdays['id']; ?>">
                                                                          <button class="delete" type="submit">Delete</button>
                                                                   </form>
                                                            </td>
                                                     </tr>
                                                 <?php endforeach; ?>

                                          </tbody>
                                   </table>
                                   <div class="col-md-4">
                                          <a href="../../../index.php">Go to home</a>
                                   </div>
                            </div>
                     </div>


                     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
                     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
                     <!-- Include all compiled plugins (below), or include individual files as needed -->
                     <script src="resource/js/bootstrap.min.js"></script>
              </div>
       </div>
</body>
</html>
