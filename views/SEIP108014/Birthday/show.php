<?php
include_once("../../../"."vendor/autoload.php");

use \App\Bitm\SEIP108014\Birthday;

$obj = new Birthday();
$bday = $obj->show($_GET['id']);
?>
<html>
    <head>
        <title>View Birthdays</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1><?php echo $bday->name;?></h1>
   <table>
        <tr>
            <td>ID: </dt>
            <td><?php echo $bday->id;?></dd> 
            
        </tr>
        <tr>
            <td>Birthday: </td>
            <td><?php echo $bday->birth_date;?></td>
        </tr>
    </table>
        <nav>
            <li><a href="index.php">Go to List</a></li>
        </nav>
    </body>
</html>

