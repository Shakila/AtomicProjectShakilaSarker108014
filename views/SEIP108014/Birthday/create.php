<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Birthday;
?>
<html>

       <head>
              <meta charset="UTF-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              
              <title>Create Birthday</title>

              <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
       </head>
       <body>
              <div class="container">
                     <div class="row">
                            <div class="col-md-4">
                                   <div class="home col-md-12">
                                          <a href="index.php"><img src="../../../images/icon-home.png"/></a>
                                   </div>
                                   <div class="col-md-12">
                                          <div class="col-md-3"></div>
                                          <div class ="col-md-6">
                                                 <h2>Birthdays</h2>
                                          </div>
                                          <div class="col-md-3"></div>
                                   </div>
                                   <div class="col-md-12">
                                          <form action="store.php" method="post" class="form-horizontal" role="form">
                                                 <input type="hidden" name="id"  /> 
                                                 <div class="form-group">
                                                        <label class="control-label col-md-6" for="name">Name:</label>
                                                        <div class="col-md-6">          
                                                               <input type="text" class="form-control" name="name" id="name" placeholder="Enter your name">
                                                        </div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label class="control-label col-md-6" for="birth_date">Birth Date:</label>
                                                        <div class="col-md-6">
                                                               <input type="date" class="form-control" name="birth_date" id="birth_date">
                                                        </div>
                                                 </div>

                                                 <div class="form-group">        
                                                        <div class="col-md-offset-2 col-md-6">
                                                               <button type="submit" class="btn btn-sucsess">Submit</button>
                                                        </div>
                                                 </div>
                                          </form>
                                   </div>
                            </div>
                     </div>
              </div>
              
             
              <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
              <script src="../../../resource/js/bootstrap.min.js"></script>
       </body>
</html>