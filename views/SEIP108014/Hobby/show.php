<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Hobby;

$obj = new Hobby();
$hob = $obj->show($_GET['id']);
?>
<html>
    <head>
        <title>View Educational level</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <table>
            <tr>
                <td>
                    <h3>ID:   <?php echo $hob->id; ?></h3>

                </td>  
            </tr>
            <tr>
                <td>
                    <?php
                    $hobbies = explode(",", $hob->title);
                    foreach ($hobbies as $hobby1) {
                        echo $hobby1 . "<br>";
                    }
                    ?>
            </tr>
    </table>
    <nav>
        <li><a href="index.php">Go to List</a></li>
    </nav>
</body>
</html>

