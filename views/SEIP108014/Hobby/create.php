<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Hobby;
?>
<html>

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create Hobby</title>
        <link rel="stylesheet" type="text/css" href="../../../css/birthdayStyle.css">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class=" col-md-12 ">
                    <div class="home col-sm-12">
                        <a href="index.php"><img src="../../../images/icon-home.png"/></a>
                    </div>

                    <div class ="title">
                        <h2>Hobby</h2>
                    </div>
                    <form action="store.php" method="post" class="form-horizontal" role="form">
                         <input type="hidden" name="id"  /> 
                        <div class="checkbox">
                            <label><input type="checkbox" name="hobbies[]" value="Reading">Reading</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" name="hobbies[]" value="Designing">Designing</label>
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" name="hobbies[]" value="Programming">Programming</label>
                        </div>
                        <div class="form-group">        
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-sucsess">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </body>
</html>