<?php
include_once("../../../" . "vendor/autoload.php");

use \App\Bitm\SEIP108014\Hobby;

$obj = new Hobby();
$var = $obj->index();
?>
<html>
    <head>
        <title>Hobby List</title>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../css/birthdayStyle.css">
        <link href="../../../resource/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">   
                <div class="col-lg-">

                    <div class ="title">
                        <h2>Hobby</h2>
                    </div>

                    <div class="col-md-6">
                        <a href="create.php">Create Hobby</a>
                    </div> 

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Serial no</th>
                                <th>Title</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $sl = 0;
                            foreach ($var as $hobby):
                                $sl++;
                                ?>
                                <tr>

                                    <td><?php echo $sl; ?></td>
                                    <td>
                                        <?php
                                        $hobbies = explode(",", $hobby['title']);
                                        foreach ($hobbies as $hobby1) {
                                            echo $hobby1 . "<br>";
                                        }
                                        ?>

                                    </td>
                                     <td><a href="edit.php?id=<?php echo $hobby['id'];?>&hobbies=<?php echo $hobby['title'];?>">Update </a><?php echo '| '; ?><a href="show.php?id=<?php echo $hobby['id'];?>">View </a><?php echo '| '; ?>
                                            <form action="delete.php" method="post">
                                                <input type="hidden" name="id" value="<?php echo $hobby['id']; ?>">
                                                <button class="delete" type="submit">Delete</button>
                                            </form>
                                        </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                        <div class="col-md-4">
                                          <a href="../../../index.php">Go to home</a>
                                   </div>
                </div>
            </div>


            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="resource/js/bootstrap.min.js"></script>
        </div>

    </body>
</html>
